#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "VehicleTelemetryComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VEHICLETELEMETRY_API UVehicleTelemetryComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	// CTOR

	UVehicleTelemetryComponent();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// BUSINESS LOGIC

	void DrawTelemetry(UCanvas* Canvas, float& YL, float& YPos);

private:

	void UpdateTelemetryTargets();

	// INTERNALS

	FString						CurrentTelemetryList;

	TArray<FString>				TelemetryArray;

	struct FTelemetryTarget
	{
		void*					TargetObject;
		FProperty*				TargetProperty;
	};

	TArray<FTelemetryTarget>	TelemetryTargets;

	TArray<TArray<float> >		TelemetryGraphValues;
	int32						TelemetryGraphIndex;
};
