// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class WannabeRacing : ModuleRules
{
	public WannabeRacing(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "ChaosVehicles", "HeadMountedDisplay", "PhysicsCore", "VehicleTelemetry" });

		PublicDefinitions.Add("HMD_MODULE_INCLUDED=1");
	}
}
