// Copyright Epic Games, Inc. All Rights Reserved.

#include "WannabeRacingGameMode.h"
#include "WannabeRacingPawn.h"
#include "WannabeRacingHud.h"

AWannabeRacingGameMode::AWannabeRacingGameMode()
{
	DefaultPawnClass = AWannabeRacingPawn::StaticClass();
	HUDClass = AWannabeRacingHud::StaticClass();
}
