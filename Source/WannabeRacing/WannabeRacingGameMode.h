// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "WannabeRacingGameMode.generated.h"

UCLASS(MinimalAPI)
class AWannabeRacingGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AWannabeRacingGameMode();
};



