#include "WannabeRacingSimData.h"

#include "WheeledVehiclePawn.h"
#include "ChaosWheeledVehicleMovementComponent.h"

#pragma optimize ("", off)

UWannabeRacingSimData::UWannabeRacingSimData()
{
	PrimaryComponentTick.bCanEverTick = true;

	bWantsInitializeComponent = true;
}

void UWannabeRacingSimData::InitializeComponent()
{
	Super::InitializeComponent();

	const AWheeledVehiclePawn* VehiclePawn = Cast<AWheeledVehiclePawn>(GetOwner());

	if (VehiclePawn != nullptr)
	{
		const UChaosVehicleMovementComponent* VehicleMovement = VehiclePawn->GetVehicleMovementComponent();
		const UChaosWheeledVehicleMovementComponent* WheeledVehicleMovement = Cast<UChaosWheeledVehicleMovementComponent>(VehicleMovement);

		if (WheeledVehicleMovement != nullptr)
		{
			int NumWheels = WheeledVehicleMovement->WheelSetups.Num();
			WheelsData.AddDefaulted(NumWheels);
		}
	}
}

void UWannabeRacingSimData::UninitializeComponent()
{
	Super::UninitializeComponent();

	WheelsData.Empty();
}

void UWannabeRacingSimData::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	const AWheeledVehiclePawn* VehiclePawn = Cast<AWheeledVehiclePawn>(GetOwner());

	if (VehiclePawn != nullptr)
	{
		const UChaosVehicleMovementComponent* VehicleMovement = VehiclePawn->GetVehicleMovementComponent();
		const UChaosWheeledVehicleMovementComponent* WheeledVehicleMovement = Cast<UChaosWheeledVehicleMovementComponent>(VehicleMovement);

		if (WheeledVehicleMovement != nullptr)
		{
			FBaseSnapshotData SnapshotData;
			WheeledVehicleMovement->GetBaseSnapshot(SnapshotData);

			ForwardSpeed = WheeledVehicleMovement->GetForwardSpeed();
			
			Speed = SnapshotData.LinearVelocity.Size();
			bIsStopped = (Speed < 10.f);

			int NumWheels = WheeledVehicleMovement->GetNumWheels();

			for (int WheelIndex = 0; WheelIndex < NumWheels; ++WheelIndex)
			{
				const FWheelStatus& WheelStatus = WheeledVehicleMovement->GetWheelState(WheelIndex);
				WheelsData[WheelIndex].bIsInContact = WheelStatus.bInContact;
			}
		}
	}
}

#pragma optimize ("", on)