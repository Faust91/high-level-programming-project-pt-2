#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WannabeRacingSimData.generated.h"

USTRUCT()
struct WANNABERACING_API FWheelSimData
{
	GENERATED_BODY()

public:

	UPROPERTY()
	float RotationSpeed = 0.f;
	UPROPERTY()
	bool bIsInContact = false;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WANNABERACING_API UWannabeRacingSimData : public UActorComponent
{
	GENERATED_BODY()

public:

	UWannabeRacingSimData();

public:	
	
	virtual void InitializeComponent() override;
	virtual void UninitializeComponent() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:

	UPROPERTY()
	float ForwardSpeed = 0.f;
	UPROPERTY()
	float Speed = 0.f;
	UPROPERTY()
	bool bIsStopped = true;
	UPROPERTY()
	TArray<FWheelSimData> WheelsData;
};