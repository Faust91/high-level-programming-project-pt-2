#include "WannabeRacingVehicle.h"

#include "WannabeRacingSimData.h"
#include "VehicleTelemetryComponent.h"

#include "DisplayDebugHelpers.h"

AWannabeRacingVehicle::AWannabeRacingVehicle()
{
	SimData = CreateDefaultSubobject<UWannabeRacingSimData>("SimData");
	Telemetry = CreateDefaultSubobject<UVehicleTelemetryComponent>("Telemetry");
}

void AWannabeRacingVehicle::DisplayDebug(UCanvas* Canvas, const FDebugDisplayInfo& DebugDisplay, float& YL, float& YPos)
{
	Super::DisplayDebug(Canvas, DebugDisplay, YL, YPos);

	static FName NAME_Telemetry = FName(TEXT("TELEMETRY"));

	if (DebugDisplay.IsDisplayOn(NAME_Telemetry))
	{
		Telemetry->DrawTelemetry(Canvas, YL, YPos);
	}
}