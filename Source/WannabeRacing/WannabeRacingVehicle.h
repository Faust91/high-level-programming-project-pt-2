#pragma once

#include "CoreMinimal.h"
#include "WannabeRacingPawn.h"
#include "WannabeRacingVehicle.generated.h"

class UWannabeRacingSimData;
class UVehicleTelemetryComponent;

UCLASS()
class WANNABERACING_API AWannabeRacingVehicle : public AWannabeRacingPawn
{
	GENERATED_BODY()

public:

	AWannabeRacingVehicle();

	UPROPERTY(Category = Vehicle, BlueprintReadOnly)
	UWannabeRacingSimData* SimData;
	UPROPERTY(Category = Vehicle, BlueprintReadOnly)
	UVehicleTelemetryComponent* Telemetry;

	virtual void DisplayDebug(UCanvas* Canvas, const FDebugDisplayInfo& DebugDisplay, float& YL, float& YPos) override;
};
