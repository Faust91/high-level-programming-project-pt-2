// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ChaosVehicleWheel.h"
#include "WannabeRacingWheelFront.generated.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS

UCLASS()
class UWannabeRacingWheelFront : public UChaosVehicleWheel
{
	GENERATED_BODY()

public:
	UWannabeRacingWheelFront();
};

PRAGMA_ENABLE_DEPRECATION_WARNINGS
