// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ChaosVehicleWheel.h"
#include "WannabeRacingWheelRear.generated.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS

UCLASS()
class UWannabeRacingWheelRear : public UChaosVehicleWheel
{
	GENERATED_BODY()

public:
	UWannabeRacingWheelRear();
};

PRAGMA_ENABLE_DEPRECATION_WARNINGS


